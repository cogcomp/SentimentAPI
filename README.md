This class takes a sentence as input and outputs its 'happiness'. 

Happiness is a categorical variables that takes values:

1 when the overall sentiment of the sentence is positive

-1 when the overall sentiment of the sentence is negative

0 when the overall sentiment of the sentence is neutral

The code uses two lexicons for sentiment computation:

1) BingLiu's lexicon at http://www.cs.uic.edu/~liub/FBS/sentiment-analysis.html

Please cite one of the following two papers:

    Minqing Hu and Bing Liu(2004). Mining and Summarizing Customer Reviews.
    Proceedings of the ACM SIGKDD International Conference on Knowledge Discovery and Data Mining
    (KDD-2004), Aug 22-25, 2004, Seattle, Washington, USA.

	Bing Liu, Minqing Hu and Junsheng Cheng (2005). Opinion Observer: Analyzing and
	Comparing Opinions on the Web. Proceedings of the 14th International World Wide Web conference
	(WWW-2005), May 10-14, 2005, Chiba, Japan.

2) MPQA lexicon

Please cite:

	Theresa Wilson, Janyce Wiebe and Paul Hoffmann (2005). Recognizing Contextual
	Polarity in Phrase-Level Sentiment Analysis. Proceedings of HLT/EMNLP 2005,
	Vancouver, Canada
------------------------------------------------------------------------------------

Example of how to use this class

public void demo() throws Exception{

	SentimentHandler sentimentHandler = new SentimentHandler();

       	String sent = "She thought she was not happy.";

        Pair<Integer, Integer> happ = sentimentHandler.getHappiness(sent);

        System.out.println("Happiness according to BingLiu Lexicon: "+happ.getFirst());

        System.out.println("Happiness according to MPQA Lexicon: "+happ.getSecond());

    }


